/*

AES - Lab 4 source code - Thread led2_control

This thread configures an OS timer that establishes a timing of 1 second. When
the timer reaches its terminal count a function is called that toggles LED3. This thread manages
control flag that is toggled every time a message is received on the queue called g_led3_queue.
The status of this control flag determines if the timer has to be activated or deactivated.
Therefore, LED3 starts toggling after 10, 30, 50, 70, ... pressings of button SW4, and
will be deactivated after 20, 40, 60, 80, ... pressings of button SW4

 */

#include "read_button.h"
#include "led2_control.h"
#include "stdio.h"

unsigned char control_counter;  // Counts the number of pressings on the SW4 button.
// Every 10 pressings the status of LED2 is toggled
unsigned char led2_status;      // Defines the status of LED2
ulong rx_msg[2];                // Message received in the queue g_led2_queue
ulong tx_msg[2];                // Message to be sent to the queue g_led3_queue
extern void initialise_monitor_handles(void);  // Used for printf outputs

/* LED2 Control entry function */
void led2_control_entry(void)
{
	/* LED type structure */
	bsp_leds_t leds;

	/* Enables printfs on the virtual console */
	initialise_monitor_handles();
	/* Initializes the internal counter */
	control_counter = 0;
	/* Initializes the status of LED2 */
	led2_status = 0;
	/* Populate the Leds structure array to simplify the use of the LEDs on the board. */
	/* No need to reach for the schematic. */
	R_BSP_LedsGet(&leds);

	while (1)
	{
		/* Wait for something to be received in the queue */
		tx_queue_receive(&g_led2_queue, rx_msg, TX_WAIT_FOREVER);
		if (led2_status == 1)
		{
			printf("LED2 Control Thread - Control counter: %u - LED2 ON\n", control_counter);
			/* Output the current output level to the LED2 connected pin */
			g_ioport.p_api->pinWrite(leds.p_leds[BSP_LED_LED2], 0x00);
		}
		else
		{
			printf("LED2 Control Thread - Control counter: %u - LED2 OFF\n", control_counter);
			/* Output the current output level to the LED2 connected pin */
			g_ioport.p_api->pinWrite(leds.p_leds[BSP_LED_LED2], 0x01);
		}
		/* Updates the internal counter */
		control_counter++;
		/* Checks if the counter has reached a value 10 */
		if (control_counter == 10)
		{
			/* Resets the counter*/
			control_counter = 0;
			/* Toggles the status of LED2 */
			if (led2_status == 1)
				led2_status = 0;
			else
				led2_status = 1;
			/* Send the message in the queue. Wait forever for space to be available in the queue for the message. */
			tx_queue_send(&g_led3_queue, tx_msg, TX_WAIT_FOREVER);
		}
	}
}
