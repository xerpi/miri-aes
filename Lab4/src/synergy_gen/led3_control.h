/* generated thread header file - do not edit */
#ifndef LED3_CONTROL_H_
#define LED3_CONTROL_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus 
extern "C" void led3_control_entry(void);
#else 
extern void led3_control_entry(void);
#endif
#ifdef __cplusplus
extern "C" {
#endif
extern TX_QUEUE g_led3_queue;
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* LED3_CONTROL_H_ */
