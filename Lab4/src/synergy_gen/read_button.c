/* generated thread source file - do not edit */
#include "read_button.h"

TX_THREAD read_button;
void read_button_create(void);
static void read_button_func(ULONG thread_input);
/** Alignment requires using pragma for IAR. GCC is done through attribute. */
#if defined(__ICCARM__)
#pragma data_alignment = BSP_STACK_ALIGNMENT
#endif
static uint8_t read_button_stack[1024] BSP_PLACE_IN_SECTION(".stack.read_button") BSP_ALIGN_VARIABLE(BSP_STACK_ALIGNMENT);
#if (8) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_external_irq_11) && !defined(SSP_SUPPRESS_ISR_ICU11)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ11);
#endif
#endif
static icu_instance_ctrl_t g_external_irq_11_ctrl;
static const external_irq_cfg_t g_external_irq_11_cfg = { .channel = 11,
		.trigger = EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = true, .pclk_div =
				EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = true, .p_callback =
				external_irq_11_callback, .p_context = &g_external_irq_11,
		.p_extend = NULL, .irq_ipl = (8), };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq_11 = { .p_ctrl =
		&g_external_irq_11_ctrl, .p_cfg = &g_external_irq_11_cfg, .p_api =
		&g_external_irq_on_icu };
TX_SEMAPHORE g_sw4_semaphore;
TX_QUEUE g_led2_queue;
static uint8_t queue_memory_g_led2_queue[8];
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;
void g_hal_init(void);

void read_button_create(void)
{
	/* Increment count so we will know the number of ISDE created threads. */
	g_ssp_common_thread_count++;

	/* Initialize each kernel object. */
	tx_semaphore_create(&g_sw4_semaphore, (CHAR *) "SW4 Semaphore", 0);
	tx_queue_create(&g_led2_queue, (CHAR *) "LED2 Queue", 1,
			&queue_memory_g_led2_queue, sizeof(queue_memory_g_led2_queue));

	tx_thread_create(&read_button, (CHAR *) "Read button", read_button_func,
			(ULONG) NULL, &read_button_stack, 1024, 1, 1, 1, TX_AUTO_START);
}

static void read_button_func(ULONG thread_input)
{
	/* Not currently using thread_input. */
	SSP_PARAMETER_NOT_USED(thread_input);

	/* First thread will take care of common initialization. */
	UINT err;
	err = tx_semaphore_get(&g_ssp_common_initialized_semaphore,
			TX_WAIT_FOREVER);

	while (TX_SUCCESS != err) {
		/* Check err, problem occurred. */
		BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
	}

	/* Only perform common initialization if this is the first thread to execute. */
	if (false == g_ssp_common_initialized) {
		/* Later threads will not run this code. */
		g_ssp_common_initialized = true;

		/* Perform common module initialization. */
		g_hal_init();

		/* Now that common initialization is done, let other threads through. */
		/* First decrement by 1 since 1 thread has already come through. */
		g_ssp_common_thread_count--;
		while (g_ssp_common_thread_count > 0) {
			err = tx_semaphore_put(&g_ssp_common_initialized_semaphore);

			while (TX_SUCCESS != err) {
				/* Check err, problem occurred. */
				BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
			}

			g_ssp_common_thread_count--;
		}
	}

	/* Initialize each module instance. */

	/* Enter user code for this thread. */
	read_button_entry();
}
