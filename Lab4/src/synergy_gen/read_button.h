/* generated thread header file - do not edit */
#ifndef READ_BUTTON_H_
#define READ_BUTTON_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus 
extern "C" void read_button_entry(void);
#else 
extern void read_button_entry(void);
#endif
#include "r_icu.h"
#include "r_external_irq_api.h"
#ifdef __cplusplus
extern "C" {
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_external_irq_11;
#ifndef external_irq_11_callback
void external_irq_11_callback(external_irq_callback_args_t *p_args);
#endif
extern TX_SEMAPHORE g_sw4_semaphore;
extern TX_QUEUE g_led2_queue;
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* READ_BUTTON_H_ */
