/*

AES - Lab 4 source code - Thread read_button

Application desciption:

The application will toggle the user LEDs present in the board. LED1 will be toggled every
time the user presses button SW4. LED2 will be toggled after 10 consecutive presses of
button SW4. LED3 will be toggled every second after 10, 30, 50, 70, ... pressings of button
SW4, and will be deactivated after 20, 40, 60, 80, ... pressings of button SW4.

The application will consist of three threads:
    - read_button thread: This thread determines if button SW4 has been pressed, and in this
      case toggles LED1. The ISR that is called when button SW4 is pressed will signal this
      situation by means of a semaphore. Every time button SW4 is toggled this thread will let
      it know to the thread that controls LED2, led2_control, by sending a message to a queue
      called g_led2_queue.

    - led2_control thread: This thread manages an internal counter that is incremented each time
      a message is written on the queue called g_led2_queue. When this counter reaches the vale 10
      it is reset, LED2 is toggled and a message is sent to a queue called g_led3_queue. Therefore,
      LED2 is toggled every 10 pressings of button SW4.

    - led3_control thread: This thread configures an OS timer that establishes a timing of 1 second. When
      the timer reaches its terminal count a function is called that toggles LED3. This thread manages
      a control flag that is toggled every time a message is received on the queue called g_led3_queue.
      The status of this control flag determines if the timer has to be activated or deactivated.
      Therefore, LED3 starts toggling after 10, 30, 50, 70, ... pressings of button SW4, and
      will be deactivated after 20, 40, 60, 80, ... pressings of button SW4.

 */

#include "read_button.h"
#include "stdio.h"

ulong sw4_state[2]; // Message sent to the queue g_led2_queue
extern void initialise_monitor_handles(void);  //Used for printf output on the virtual console

/* Read Button entry function */
void read_button_entry(void)
{
	/* LED type structure */
	bsp_leds_t leds;
	/* Storage of LED1 on the board's output level */
	/* A value 0x01 forces the LED to OFF, a value
    0x00 forces the LED to ON */
	ioport_level_t led_1_level = 0x01;

	/* Enables printfs on the virtual console */
	initialise_monitor_handles();
	/* Populate the Leds structure array to simplify the use of the LEDs on the board. */
	/* No need to reach for the schematic. */
	R_BSP_LedsGet(&leds);
	/* Open and configure the external IRQ pin connected to SW4 on the board */
	g_external_irq_11.p_api->open(g_external_irq_11.p_ctrl, g_external_irq_11.p_cfg);
	/* Turn OFF the three LEDs on the board */
	g_ioport.p_api->pinWrite(leds.p_leds[BSP_LED_LED1], 0x01);
	g_ioport.p_api->pinWrite(leds.p_leds[BSP_LED_LED2], 0x01);
	g_ioport.p_api->pinWrite(leds.p_leds[BSP_LED_LED3], 0x01);

	while (1)
	{
		/* Output the current output level to the LED1 connected pin */
		g_ioport.p_api->pinWrite(leds.p_leds[BSP_LED_LED1], led_1_level);
		/* Identify the status of the LED */
		if (led_1_level == 0x00)
		{
			printf("Read Button Thread - LED1 ON\n");
		}
		else
		{
			printf("Read Button Thread - LED1 OFF\n");
		}
		/* Toggle the pin level */
		led_1_level = (led_1_level == 0x01) ? 0x00 : 0x01;

		/* Wait forever for the semaphore from the IRQ11 ISR callback to be posted */
		/* The RTOS will suspend this thread until this event occurs */
		tx_semaphore_get(&g_sw4_semaphore, TX_WAIT_FOREVER);
		/* Send the message in the queue. Wait forever for space to be available in the queue for the message */
		tx_queue_send(&g_led2_queue, sw4_state, TX_WAIT_FOREVER);
	}
}

/* Callback function called by the external IRQ11 ISR */
void external_irq_11_callback(external_irq_callback_args_t *p_args)
{
	/* Post to the semaphore to indicate SW4 has been pressed. */
	tx_semaphore_put(&g_sw4_semaphore);
}

