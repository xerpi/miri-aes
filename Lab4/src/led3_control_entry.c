/*
AES - Lab 4 source code - Thread led3_control

This thread configures an OS timer that establishes a timing of 1 second. When
the timer reaches its terminal count a function is called that toggles LED3. This thread manages
control flag that is toggled every time a message is received on the queue called g_led3_queue.
The status of this control flag determines if the timer has to be activated or deactivated.
Therefore, LED3 starts toggling after 10, 30, 50, 70, ... pressings of button SW4, and
will be deactivated after 20, 40, 60, 80, ... pressings of button SW4
 */

#include "led2_control.h"
#include "led3_control.h"
#include "stdio.h"

unsigned char led3_status;      // Defines the status of LED3
unsigned char timer_status;     // Defines the status of the timer
ulong rx_msg_led3[2];           // Message received in the queue g_led3_queue
TX_TIMER   led3_timer;          // Timer declaration
/* LED type structure */
bsp_leds_t leds;
extern void initialise_monitor_handles(void);  // Used for printf outputs

/* Function called whenever the timer reaches its limit value */
void led3_toggle(ulong invalue)
{
	if (led3_status) {
		printf("LED3 Control Thread - LED3 ON\n");
		g_ioport.p_api->pinWrite(leds.p_leds[BSP_LED_LED3], 0x00);
	} else {
		printf("LED3 Control Thread - LED3 OFF\n");
		g_ioport.p_api->pinWrite(leds.p_leds[BSP_LED_LED3], 0x01);
	}

	led3_status = !led3_status;
}

/* LED3 Control entry function */
void led3_control_entry(void)
{
	/* Enables printfs on the virtual console */
	initialise_monitor_handles();
	/* Populate the Leds structure array to simplify the use of the LEDs on the board. */
	/* No need to reach for the schematic. */
	R_BSP_LedsGet(&leds);

	/* Initializes the status of LED3 */
	led3_status = 0;
	/* Initializes the status of timer */
	timer_status = 0;
	/* Create the timer */
	tx_timer_create(&led3_timer, "led3_timer", led3_toggle, 0, 100, 100, TX_NO_ACTIVATE);

	while (1) {
		/* Wait for something to be received in the queue */
		tx_queue_receive(&g_led3_queue, rx_msg_led3, TX_WAIT_FOREVER);

		if (timer_status)
			tx_timer_deactivate(&led3_timer);
		else
			tx_timer_activate(&led3_timer);

		timer_status = !timer_status;
	}

	tx_timer_delete(&led3_timer);
}

