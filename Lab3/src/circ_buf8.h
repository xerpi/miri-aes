#ifndef CIRC_BUF8_H
#define CIRC_BUF8_H

#include <stdint.h>

typedef struct {
	uint32_t capacity;
	uint32_t head;
	uint32_t tail;
	uint32_t size;
	uint8_t *data;
} circ_buf8_t;

void circ_buf8_init(circ_buf8_t *buf, uint8_t *data, uint32_t capacity);
uint32_t circ_buf8_get_head(const circ_buf8_t *buf);
uint32_t circ_buf8_get_tail(const circ_buf8_t *buf);
uint32_t circ_buf8_get_size(const circ_buf8_t *buf);
uint32_t circ_buf8_get_capacity(const circ_buf8_t *buf);
void circ_buf8_push(circ_buf8_t *buf, uint8_t element);
uint8_t circ_buf8_pop(circ_buf8_t *buf);
uint8_t circ_buf8_get(const circ_buf8_t *buf, uint32_t index);

#define circ_buf8_for_each(pos, buf, tmp)		\
		for (tmp = 0, pos = (buf)->tail;		\
			 tmp != (buf)->size;				\
			 tmp++,								\
			 pos = (pos + 1) % (buf)->capacity)	\

#endif
