/* generated thread header file - do not edit */
#ifndef LAB3_THREAD_H_
#define LAB3_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus 
extern "C" void lab3_thread_entry(void);
#else 
extern void lab3_thread_entry(void);
#endif
#ifdef __cplusplus
extern "C" {
#endif
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* LAB3_THREAD_H_ */
