#include "circ_buf8.h"

void circ_buf8_init(circ_buf8_t *buf, uint8_t *data, uint32_t capacity)
{
	buf->capacity = capacity;
	buf->head = 0;
	buf->tail = 0;
	buf->size = 0;
	buf->data = data;
}

uint32_t circ_buf8_get_head(const circ_buf8_t *buf)
{
	return buf->head;
}

uint32_t circ_buf8_get_tail(const circ_buf8_t *buf)
{
	return buf->tail;
}

uint32_t circ_buf8_get_size(const circ_buf8_t *buf)
{
	return buf->size;
}

uint32_t circ_buf8_get_capacity(const circ_buf8_t *buf)
{
	return buf->capacity;
}

void circ_buf8_push(circ_buf8_t *buf, uint8_t element)
{
	buf->data[buf->head] = element;
	buf->head = (buf->head + 1) % buf->capacity;

	if (buf->size < buf->capacity)
		buf->size++;
	else
		buf->tail = (buf->tail + 1) % buf->capacity;
}

uint8_t circ_buf8_pop(circ_buf8_t *buf)
{
	uint8_t element = buf->data[buf->tail];
	buf->tail = (buf->tail + 1) % buf->capacity;
	return element;
}

uint8_t circ_buf8_get(const circ_buf8_t *buf, uint32_t index)
{
	return buf->data[index];
}
