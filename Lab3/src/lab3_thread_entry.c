#include <circ_buf8.h>
#include <stdio.h>
#include <string.h>
#include "lab3_thread.h"

#define SW_4 IOPORT_PORT_00_PIN_06
#define SW_5 IOPORT_PORT_00_PIN_05

#define LED1 BSP_LED_LED1
#define LED2 BSP_LED_LED2

/* Set the blink frequency (must be <= BSP_DELAY_UNITS_MILLISECONDS */
#define FREQ_IN_HZ 10

extern void initialise_monitor_handles(void);
static void halt(void) __attribute__((noreturn));
static int can_tx1(can_id_t id, uint8_t data);
static int can_tx(can_id_t ID, uint8_t *frame, uint8_t data_length);
static int can_rx(void);
static uint8_t rand_get8(void);
static void clear_leds(void);
static void set_leds(void);
static void blink_led(uint8_t ledn);

static bsp_leds_t leds;
static uint8_t tx_data[8], rx_data[8];
static can_frame_t rx_frame, tx_frame;
static bool rx_flag = false, tx_flag = false;
static ioport_level_t sw4pin = IOPORT_LEVEL_LOW, sw5pin = IOPORT_LEVEL_LOW;

/* Circular buffer */
#define CIRC_BUF_CAPACITY 16
static circ_buf8_t circ_buf;
static uint8_t circ_buf_data[CIRC_BUF_CAPACITY];

void lab3_thread_entry(void)
{
	ssp_err_t err;

	initialise_monitor_handles();

	printf("AES Lab3 init\n");

	R_BSP_LedsGet(&leds);

	g_sce.p_api->open(g_sce.p_ctrl, g_sce.p_cfg);
	g_sce_trng.p_api->open(g_sce_trng.p_ctrl, g_sce_trng.p_cfg);

	err = g_can0.p_api->open(g_can0.p_ctrl, g_can0.p_cfg);
	if (err != SSP_SUCCESS) {
		printf("\n CAN 0 Open error: %d",err);
		halt();
	}

	circ_buf8_init(&circ_buf, circ_buf_data, CIRC_BUF_CAPACITY);

	clear_leds();

	while (1) {
		g_ioport.p_api->pinRead(SW_4, &sw4pin);
		g_ioport.p_api->pinRead(SW_5, &sw5pin);

		if (sw4pin == IOPORT_LEVEL_LOW) {
			if (can_tx1(0x7ff, rand_get8()))
				blink_led(LED1);
		} else if (sw5pin == IOPORT_LEVEL_LOW) {
			if (can_tx1(0x7fe, rand_get8()))
					blink_led(LED1);
		} else if (can_rx()) {
			blink_led(LED2);
			circ_buf8_push(&circ_buf, rx_data[0]);

			uint16_t sum = 0;
			uint8_t avg;
			uint32_t pos, tmp;

			circ_buf8_for_each(pos, &circ_buf, tmp) {
				/* printf("circ_buf[%ld]: %ld\n", pos, circ_buf8_get(&circ_buf, pos)); */
				sum += circ_buf8_get(&circ_buf, pos);
			}

			avg = sum / circ_buf8_get_size(&circ_buf);

			printf("Buf size: %ld\n", circ_buf8_get_size(&circ_buf));
			printf("Average: %ld\n", avg);
		}
	}

	printf("\nThread end.\n");
	halt();
}

int can_tx1(can_id_t id, uint8_t data)
{
	tx_data[0] = data;
	return can_tx(id, tx_data, 1);
}

int can_tx(can_id_t id, uint8_t *frame, uint8_t data_length)
{
	ssp_err_t err;

	/* Initialize CAN transmission frame */
	tx_frame.type = CAN_FRAME_TYPE_DATA;
	tx_frame.id = id;
	memcpy(&tx_frame.data, frame, sizeof(tx_data));
	tx_frame.data_length_code = data_length;

	/* Start CAN transmission */
	err = g_can0.p_api->write(g_can0.p_ctrl, 0, &tx_frame);
	if(err != SSP_SUCCESS) {
		printf("\n CAN 0 Write error: %d", err);
		halt();
	}

	printf("\nTX ID: %d \tTX Data: %d\n", (int)tx_frame.id,tx_frame.data[0]);

	/* Wait for transmission to be completed */
	while (!tx_flag)
			;

	/* Transmission done. Clear tx_flag. */
	tx_flag = false;
	return 1;
}

int can_rx()
{
	/* Check for received CAN data */
	if (!rx_flag)
		return 0;

	/* Store received frame data */
	memcpy(&rx_data, &rx_frame.data, sizeof(rx_data));

	printf("\nRX flag: %d \tID: %d \tReceived Data: %d\n",rx_flag, (int)rx_frame.id, rx_data[0]);

	rx_flag = false;
	return 1;
}

void can_callback(can_callback_args_t * p_args)
{
	ssp_err_t err;

	if (p_args->channel != 0) {
		printf("\nInvalid CAN call back channel, '0' expected\n");
		halt();
	}

	switch(p_args->event) {
	case CAN_EVENT_RX_COMPLETE:
		err = g_can0.p_api->read (g_can0.p_ctrl, p_args->mailbox, &rx_frame);
		if (err != SSP_SUCCESS) {
			printf("\n CAN 0 Read error: %d", err);
			halt();
		}
		rx_flag = true;
		break;
	case CAN_EVENT_TX_COMPLETE:
		tx_flag = true;
		break;
	default:
		printf("Unexpected can event: %d\n", p_args->event);
		break;
	}
}

uint8_t rand_get8(void)
{
	uint32_t data;
	g_sce_trng.p_api->read(g_sce_trng.p_ctrl, &data, 1);
	return (uint8_t)data;
}

void clear_leds(void)
{
	g_ioport.p_api->pinWrite(leds.p_leds[LED1], IOPORT_LEVEL_LOW);
	g_ioport.p_api->pinWrite(leds.p_leds[LED2], IOPORT_LEVEL_LOW);
}

void set_leds(void)
{
	g_ioport.p_api->pinWrite(leds.p_leds[LED1], IOPORT_LEVEL_HIGH);
	g_ioport.p_api->pinWrite(leds.p_leds[LED2], IOPORT_LEVEL_HIGH);
}

void blink_led(uint8_t ledn)
{
	/* Calculate the delay in terms of bsp_delay_units */
	static uint32_t delay = BSP_DELAY_UNITS_MILLISECONDS/FREQ_IN_HZ;

	g_ioport.p_api->pinWrite(leds.p_leds[ledn], IOPORT_LEVEL_LOW);
	R_BSP_SoftwareDelay(delay, BSP_DELAY_UNITS_MILLISECONDS); /* Delay */
	g_ioport.p_api->pinWrite(leds.p_leds[ledn], IOPORT_LEVEL_HIGH);
	R_BSP_SoftwareDelay(delay, BSP_DELAY_UNITS_MILLISECONDS); /* Delay */
}

void halt(void)
{
	printf("\n--- HALTED ---\n");

	while (1)
		tx_thread_sleep(1);
}
