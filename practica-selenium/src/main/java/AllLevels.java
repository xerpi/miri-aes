import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.JavascriptExecutor;

public class AllLevels {
	
	private static WebDriver driver = null;
	private String mainWindowHandle = null;
	private By startButton = By.id("start_button");
	private By levelTitle=By.cssSelector("h1");
	private By input = By.id("input");
	private By nextButton = By.id("next");

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ArrayList<String> optionsList = new ArrayList<String>();
		ChromeOptions chromeOptions = new ChromeOptions();
		optionsList.add("--start-maximized");
		optionsList.add("--incognito");
		chromeOptions.addArguments(optionsList);
		driver = new ChromeDriver(chromeOptions);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//Close browser
		//driver.close();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws InterruptedException {
		
		//Open URL
		driver.get("http://104.248.193.22/pruebaSelenium/");
		WebElement levelTitleElement = driver.findElement(levelTitle);	
		
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		//WebElement clickButton=wait.until(ExpectedConditions.visibilityOfElementLocated(startButton));	
		
		assertEquals(levelTitleElement.getText(), "Pr�ctica Selenium");
		
		//Level 1
		WebElement startButtonElement = driver.findElement(startButton);
		startButtonElement.click();
		levelTitleElement = driver.findElement(levelTitle);
		assertEquals(levelTitleElement.getText(), "Level 2");
		
		//Level 2
		WebElement lvl2InputText = driver.findElement(input);
		WebElement lvl2ContinueButton = driver.findElement(nextButton);
		lvl2InputText.sendKeys("selenium");
		lvl2ContinueButton.click();
		levelTitleElement = driver.findElement(levelTitle);
		assertEquals(levelTitleElement.getText(), "Level 3");
		
		//Level 3
		WebElement lvl3Token = driver.findElement(By.xpath("//*[@id=\"main\"]/div/label"));
		WebElement lvl3InputText = driver.findElement(input);
		WebElement lvl3ContinueButton = driver.findElement(nextButton);
		lvl3InputText.sendKeys(lvl3Token.getText());
		lvl3ContinueButton.click();
		levelTitleElement = driver.findElement(levelTitle);
		assertEquals(levelTitleElement.getText(), "Level 4");		
		
		//Level 4
		List<WebElement> lv4Buttons = driver.findElements(By.className("btn"));
		for (WebElement e: lv4Buttons)
			e.click();
		levelTitleElement = driver.findElement(levelTitle);
		assertEquals(levelTitleElement.getText(), "Level 5");		
				
		//Level 5
		WebElement lvl5Href = driver.findElement(By.xpath("//*[@id=\"main\"]/div/div/div/span/table/tbody/tr/td/a"));
		lvl5Href.click();
		levelTitleElement = driver.findElement(levelTitle);
		assertEquals(levelTitleElement.getText(), "Level 6");		

		//Level 6
		((JavascriptExecutor)driver).executeScript("document.getElementById('hidden\"').click();");
		//System.out.println(driver.getCurrentUrl());
		//assertEquals(driver.getCurrentUrl().contains("index7.html"), true);	

		//Level 7
		mainWindowHandle = driver.getWindowHandle();
		driver.switchTo().alert().accept();	
		driver.switchTo().window(mainWindowHandle);
		
		//Level 8
		mainWindowHandle = driver.getWindowHandle();
		driver.switchTo().alert().sendKeys("9");
		driver.switchTo().alert().accept();
		driver.switchTo().window(mainWindowHandle);
		
		//Level 9
		mainWindowHandle = driver.getWindowHandle();
		
		/* Wait until the popup is raised */
	    while (driver.getWindowHandles().size() == 1)
	        Thread.sleep(500);
	    
	    /* Get popup window handle */
		String popUpHandle = null;
		Set<String> windowHandles = driver.getWindowHandles();
		for (String handle: windowHandles) {
			System.out.println(handle);
			if (!handle.equals(driver.getWindowHandle())) {
				popUpHandle = handle;
				break;
			}
		}
		assertNotEquals(popUpHandle, null);
		
		/* Get password from popup */
		driver.switchTo().window(popUpHandle);
		String password = driver.findElement(By.id("pass")).getText();
		System.out.println(password);
		
		driver.switchTo().window(mainWindowHandle);
		
		WebElement lvl9InputText = driver.findElement(input);
		WebElement lvl9ContinueButton = driver.findElement(nextButton);
		lvl9InputText.sendKeys(password);
		lvl9ContinueButton.click();
		levelTitleElement = driver.findElement(levelTitle);
		assertEquals(levelTitleElement.getText(), "Level 10");
		
		//Level 10
		WebElement lv10SourceWidget = driver.findElement(By.id("source"));
		WebElement lv10TargetWidget = driver.findElement(By.id("target"));
		
		Actions builder = new Actions(driver);
		Action dragAndDrop = builder.clickAndHold(lv10SourceWidget)
		   .moveToElement(lv10TargetWidget)
		   .release(lv10TargetWidget)
		   .build();
		dragAndDrop.perform();
	}
}
