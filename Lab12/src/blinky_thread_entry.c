#include "blinky_thread.h"
#include "stdio.h"
#include "stdlib.h"

extern void initialise_monitor_handles(void);

void rand_init32(uint32_t *a, uint32_t n);
int sort32_compare(const void *a, const void *b);
void sort32(uint32_t *a, uint32_t n);

const uint32_t N = 32;

/* Initializes buffer with random data */
void rand_init32(uint32_t *a, uint32_t n)
{
	g_sce_trng.p_api->read(g_sce_trng.p_ctrl, a, n);
}

/* qsort compare callback */
int sort32_compare(const void *a, const void *b)
{
	if (*(uint32_t *)a > *(uint32_t *)b)
		return 1;
	else if (*(uint32_t *)a < *(uint32_t *)b)
		return -1;
	return 0;
}

/* Sorts by using libc's qsort function */
void sort32(uint32_t *a, uint32_t n)
{
	qsort(a, n, sizeof(uint32_t), sort32_compare);
}

/* Returns 1 if properly sorted, 0 otherwise. */
static int checker32(uint32_t *a, uint32_t n)
{
	uint32_t i;
	for (i = 0; i < n - 1; i++) {
		if (a[i] > a[i + 1])
			return 0;
	}
	return 1;
}

void blinky_thread_entry(void)
{
	/* LED type structure */
	bsp_leds_t leds;
	/* LED state variable */
	ioport_level_t level = IOPORT_LEVEL_HIGH;
	/* Random data */
	uint32_t a[N];

	/* Used for printf outputs */
	initialise_monitor_handles();

	/* Open the secure *crypto* engine driver */
	g_sce.p_api->open(g_sce.p_ctrl, g_sce.p_cfg);
	/* Open the TRNG driver */
	g_sce_trng.p_api->open(g_sce_trng.p_ctrl, g_sce_trng.p_cfg);

	/* Get LED information for this board */
	R_BSP_LedsGet(&leds);

	/* Initialize buffer with random data */
	rand_init32(a, N);

	/* Update all board LEDs to OFF */
	for (uint32_t i = 0; i < leds.led_count; i++)
		g_ioport.p_api->pinWrite(leds.p_leds[i], IOPORT_LEVEL_HIGH);

	/* Orange LED */
	g_ioport.p_api->pinWrite(IOPORT_PORT_06_PIN_02, IOPORT_LEVEL_LOW);

	/* Read N random 32-bit words */
	sort32(a, N);

	/* Check if they are properly sorted */
	if (checker32(a, N))
		g_ioport.p_api->pinWrite(IOPORT_PORT_06_PIN_00, IOPORT_LEVEL_LOW);
	else
		g_ioport.p_api->pinWrite(IOPORT_PORT_06_PIN_01, IOPORT_LEVEL_LOW);

	while (1)
		tx_thread_sleep(1000);
}
