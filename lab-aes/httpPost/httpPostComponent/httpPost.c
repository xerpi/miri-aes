

#include "legato.h"
#include "le_timer.h"
#include "interfaces.h"
#include <curl/curl.h>
#include <time.h>

#define TIMEOUT_SECS    10
#define SSL_ERROR_HELP    "Make sure your system date is set correctly (e.g. `date -s '2016-7-7'`)"
#define POST_MESSAGE "Hello from TCU %s"
static const char * Url = "http://ptsv2.com/t/ay5n2-1550045232/post";


static void GetUrl ( void )
{
    CURL *curl;
    CURLcode res;
    char message[32];
    char imei[LE_INFO_IMEI_MAX_BYTES];
    le_result_t result = LE_FAULT;

    result = le_info_GetImei(imei, sizeof(imei));
    if (result == LE_OK)
    {
        snprintf(message, sizeof(message), POST_MESSAGE, imei);
        LE_INFO("Imei retrieved: %s", imei);
    }

    curl = curl_easy_init();
    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, Url);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, message);
        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
        {
            LE_ERROR("curl_easy_perform() failed: %s", curl_easy_strerror(res));
            if (res == CURLE_SSL_CACERT)
            {
                LE_ERROR(SSL_ERROR_HELP);
            }
        }
        LE_INFO("POST was successfull!");
        curl_easy_cleanup(curl);
    }
    else
    {
        LE_ERROR("Couldn't initialize cURL.");
    }

    curl_global_cleanup();
}

static void TimeoutHandler ( le_timer_Ref_t timerRef)
{
    GetUrl();
}

COMPONENT_INIT
{
    printf("HTTP Post!\n");

    le_timer_Ref_t timerPtr = le_timer_Create("Connection timeout timer");
    le_clk_Time_t interval = {TIMEOUT_SECS, 0};
    le_timer_SetInterval(timerPtr, interval);
    le_timer_SetHandler(timerPtr, &TimeoutHandler);
    le_timer_Start(timerPtr);

}
