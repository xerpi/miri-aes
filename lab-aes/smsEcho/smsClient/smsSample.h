
#ifndef SMS_SAMPLE_INCLUDE_GUARD
#define SMS_SAMPLE_INCLUDE_GUARD

#include "legato.h"

le_result_t smsmo_SendMessage
(
    const char*   destinationPtr,
    const char*   textPtr
);

le_result_t smsmt_Receiver( void );

le_result_t smsmt_MonitorStorage( void );

void smsmt_HandlerRemover( void );

void smsmt_StorageHandlerRemover( void );

#endif
