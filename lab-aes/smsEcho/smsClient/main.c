
#include "legato.h"
#include "interfaces.h"
#include "smsSample.h"

COMPONENT_INIT
{
    LE_INFO("Start SMS Sample!");
    smsmt_Receiver();
    smsmt_MonitorStorage();
}
