#ifndef HTTP_INCLUDE_GUARD
#define HTTP_INCLUDE_GUARD

#include "legato.h"

void http_get_url(const char *url, const char *message);

#endif
